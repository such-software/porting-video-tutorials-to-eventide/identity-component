require "eventide/postgres"

require "send_email_component"

require "identity_component/load"

require "identity_component/identity"
require "identity_component/projection"
require "identity_component/store"
require "identity_component/email_id"
require "identity_component/render_email"

require "identity_component/handlers/commands"
require "identity_component/handlers/events"

require "identity_component/consumers/commands"
require "identity_component/consumers/events"

require "identity_component/start"
