module IdentityComponent
  module Messages
    module Events
      class Registered
        include Messaging::Message

        attribute :identity_id, String
        attribute :email, String
        attribute :password_hash, String
      end
    end
  end
end
