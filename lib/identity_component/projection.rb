module IdentityComponent
  class Projection
    include EntityProjection
    include Messages::Events

    entity_name :identity

    apply Registered do |registered|
      SetAttributes.(identity, registered, copy: [
        { :identity_id => :id }
      ])
    end
  end
end
