module IdentityComponent
  module Commands
    class Register
      include Dependency
      include Messaging::StreamName

      dependency :write, Messaging::Postgres::Write

      category :identity

      def self.build
        new.tap do |instance|
          instance.configure
        end
      end

      def self.call(identity_id:, email:, password_hash:)
        instance = build

        instance.(identity_id:, email:, password_hash:)
      end

      def configure
        Messaging::Postgres::Write.configure(self)
      end

      def call(identity_id:, email:, password_hash:)
        register = Messages::Commands::Register.build

        register.identity_id = identity_id
        register.email = email
        register.password_hash = password_hash

        stream_name = command_stream_name(identity_id)

        write.(register, stream_name)
      end
    end
  end
end
