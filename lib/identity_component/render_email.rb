module IdentityComponent
  module RenderEmail
    class Email
      def subject
        "Welcome to the site!"
      end

      def text
        "Welcome to the site!"
      end

      def html
        "<h1>Welcome to the site!</h1>"
      end
    end

    def self.call(to)
      Email.new
    end
  end
end
