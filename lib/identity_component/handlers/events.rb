#module IdentityComponent
#  module RenderEmail
#    class Email
#      def subject
#      end
#
#      def text
#      end
#
#      def html
#      end
#    end
#
#    def self.call
#      Email.new
#    end
#  end
#end

module IdentityComponent
  module Handlers
    class Events
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency

      include Messages::Events
    
      dependency :write, Messaging::Postgres::Write

      handle Registered do |registered|
        email_id = EmailId.(registered.email)
        rendered_email = RenderEmail.(registered.email)

        send = SendEmailComponent::Messages::Commands::Send.follow(
          registered,
          include: [:email => :to]
        )

        send.email_id = email_id
        send.subject = rendered_email.subject
        send.text = rendered_email.text
        send.html = rendered_email.html

        command_stream_name = "sendEmail:command-#{email_id}"

        write.(send, command_stream_name)
      end
    end
  end
end
