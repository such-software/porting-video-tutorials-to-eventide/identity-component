module IdentityComponent
  module Handlers
    class Commands
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency

      include Messages::Commands
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :store, Store

      def configure
        Messaging::Postgres::Write.configure(self)
        Store.configure(self)
      end

      category :identity

      handle Register do |register|
        identity_id = register.identity_id

        identity, version = store.fetch(identity_id, include: :version)

        if identity.registered?
          logger.info(tag: :ignored) { "Command ignored (Command: #{registered.message_type}, Identity ID: #{identity_id})" }
          return
        end

        registered = Registered.follow(register)

        stream_name = stream_name(identity_id)

        write.(registered, stream_name, expected_version: version)
      end
    end
  end
end
