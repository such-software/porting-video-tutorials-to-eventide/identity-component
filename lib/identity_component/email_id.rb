require "uuidtools"

module IdentityComponent
  module EmailId
    EMAIL_NAMESPACE = UUIDTools::UUID.parse("0c46e0b7-dfaf-443a-b150-053b67905cc2")

    def self.call(email)
      uuid = UUIDTools::UUID.sha1_create(EMAIL_NAMESPACE, email)

      uuid.to_s
    end
  end
end
