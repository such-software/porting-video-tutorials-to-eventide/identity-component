require "clock/controls"
require "identifier/uuid/controls"

require "identity_component/controls/id"
require "identity_component/controls/time"
require "identity_component/controls/version"
require "identity_component/controls/email"
require "identity_component/controls/password_hash"

require "identity_component/controls/identity"

require "identity_component/controls/commands/register"

require "identity_component/controls/events/registered"
