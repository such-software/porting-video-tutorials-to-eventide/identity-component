module IdentityComponent
  class Identity
    include Schema::DataStructure

    attribute :id, String
    attribute :email, String

    def registered?
      !!id
    end
  end
end
