# Component initiator user guide: http://docs.eventide-project.org/user-guide/component-host.html#component-initiator

module IdentityComponent
  module Start
    def self.call
      Consumers::Commands.start("identity:command")
      Consumers::Events.start("identity")
    end
  end
end
