module IdentityComponent
  module Controls
    module Identity
      def self.example
        Registered.example
      end

      module New
        def self.example
          IdentityComponent::Identity.build
        end
      end

      module Registered
        def self.example
          identity = New.example

          identity.id = Identity.id

          identity
        end
      end
    end
  end
end
