module IdentityComponent
  module Controls
    module Events
      module Registered
        def self.example
          registered = Messages::Events::Registered.build

          registered.identity_id = Controls::ID.example
          registered.email = Controls::Email.example
          registered.password_hash = Controls::PasswordHash.example

          registered
        end
      end
    end
  end
end
