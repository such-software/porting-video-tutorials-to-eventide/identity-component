module IdentityComponent
  module Controls
    module Commands
      module Register
        def self.example
          register =  Messages::Commands::Register.build

          register.identity_id = ID.example
          register.email = Email.example
          register.password_hash = PasswordHash.example

          register
        end
      end
    end
  end
end
