module IdentityComponent
  module Controls
    module PasswordHash
      def self.example
        "$2b$10$/Q8DLRSWY4pmHhU.jX.Ft.YN5yna.rD7eJkwvudAStftwLQIH865W"
      end
    end
  end
end
