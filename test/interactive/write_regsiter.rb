require_relative "./interactive_init"

identity_id = Controls::ID.example
email = Controls::Email.example
password_hash = Controls::PasswordHash.example

Commands::Register.(identity_id:, email:, password_hash:)
