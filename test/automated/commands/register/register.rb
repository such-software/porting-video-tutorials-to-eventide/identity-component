require_relative "../../automated_init"

context "Commands" do
  context "Register" do
    context "Register" do
      identity_id = Controls::ID.example
      email = Controls::Email.example
      password_hash = Controls::PasswordHash.example

      register = Commands::Register.new

      register.(identity_id:, email:, password_hash:)

      writer = register.write

      register_command = writer.one_message do |command|
        command.instance_of?(Messages::Commands::Register)
      end

      test "Register command is written" do
        refute(register_command.nil?)
      end

      test "Written to the identity:command stream" do
        written_to_stream = writer.written?(register_command) do |stream_name|
          stream_name == "identity:command-#{identity_id}"
        end

        assert(written_to_stream)
      end

      context "attributes" do
        test "identity_id" do
          assert(register_command.identity_id == identity_id)
        end

        test "email" do
          assert(register_command.email == email)
        end

        test "password_hash" do
          assert(register_command.password_hash == password_hash)
        end
      end
    end
  end
end
