require_relative "../automated_init"

context "Projection" do
  context "Registered" do
    registered = Controls::Events::Registered.example

    identity_id = registered.identity_id or fail

    identity = Controls::Identity::New.example

    Projection.(identity, registered)

    context "attributes" do
      test "id" do
        assert(identity.id == identity_id)
      end
    end
  end
end
