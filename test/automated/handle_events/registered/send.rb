require_relative "../../automated_init.rb"

context "Handle Events" do
  context "Registered"do
    context "Send" do
      handler = Handlers::Events.new

      registered = Controls::Events::Registered.example

      email = registered.email or fail

      email_id = EmailId.(email)

      rendered_email = RenderEmail.(email)

      subject = rendered_email.subject or fail
      text = rendered_email.text or fail
      html = rendered_email.html or fail

      handler.(registered)

      writer = handler.write

      send = writer.one_message do |command|
        command.instance_of?(SendEmailComponent::Messages::Commands::Send)
      end

      test "Send command was written" do
        refute(send.nil?)
      end

      test "Send command written to a sendEmail:command stream" do
        written_to_stream = writer.written?(send) do |stream_name|
          stream_name == "sendEmail:command-#{email_id}"
        end

        assert(written_to_stream)
      end

      context "Attributes" do
        test "emailId" do
          assert(send.email_id == email_id)
        end

        test "to" do
          assert(send.to == email)
        end

        test "subject" do
          assert(send.subject == subject)
        end

        test "text" do
          assert(send.text == text)
        end

        test "html" do
          assert(send.html == html)
        end
      end
    end
  end
end
