require_relative "../../automated_init"

context "Handle Commands" do
  context "Register" do
    context "Registered" do
      handler = Handlers::Commands.new

      register = Controls::Commands::Register.example

      identity_id = register.identity_id or fail
      email = register.email or fail
      password_hash = register.password_hash or fail

      handler.(register)

      writer = handler.write

      registered = writer.one_message do |message|
        message.instance_of?(Messages::Events::Registered)
      end

      test "Registered event is written" do
        refute(registered.nil?)
      end

      test "It was written to an identity stream" do
        written_to_stream = writer.written?(registered) do |stream_name|
          stream_name == "identity-#{identity_id}"
        end

        assert written_to_stream
      end

      context "attributes" do
        test "identity_id" do
          assert(registered.identity_id == identity_id)
        end

        test "email" do
          assert(registered.email == email)
        end

        test "password_hash" do
          assert(registered.password_hash == password_hash)
        end
      end
    end
  end
end
