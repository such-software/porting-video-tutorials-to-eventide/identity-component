require_relative "../../automated_init"

context "Handle Commands" do
  context "Register" do
    context "Expected version" do
      handler = Handlers::Commands.new

      register = Controls::Commands::Register.example

      identity_id = register.identity_id or fail

      identity = Controls::Identity::New.example

      version = Controls::Version.example

      handler.store.add(identity_id, identity, version)

      handler.(register)

      writer = handler.write

      registered = writer.one_message do |event|
        event.instance_of?(Messages::Events::Registered)
      end

      test "Written with the expected version" do
        written_to_stream = writer.written?(registered) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end

