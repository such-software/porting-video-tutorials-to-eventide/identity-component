require_relative "../../automated_init"

context "Handle Commands" do
  context "Register" do
    context "Ignored" do
      handler = Handlers::Commands.new

      register = Controls::Commands::Register.example

      identity_id = register.identity_id or fail

      identity = Controls::Identity::Registered.example

      handler.store.add(identity_id, identity)

      handler.(register)

      writer = handler.write

      registered = writer.one_message do |event|
        event.instance_of?(Messages::Events::Registered)
      end
      
      test "Registered was not written" do
        assert(registered.nil?)
      end
    end
  end
end
