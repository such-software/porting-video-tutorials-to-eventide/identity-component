require_relative "./automated_init"

context "EmailId" do
  email = Controls::Email.example

  email_id = EmailId.(email)

  # Hand generated this. If email's value changes, this test
  # will start failing.
  expected_email_id = "4c4d0f42-ed54-5d96-9929-f3e9563c016d"

  test "It correctly converts the email to an ID" do
    assert(email_id == expected_email_id)
  end
end
